.PHONY: build test release install readme doc clean version
.DEFAULT_GOAL := build

t ?=
jobs := $(shell echo $$(($$(nproc) * 2)))

build:
	cargo build -j $(jobs)

test:
	cargo test --no-fail-fast $(t) -- --nocapture

release: test
	cargo build -j $(jobs) --release

doc:
	cargo doc -j $(jobs)

install: test
	cargo install -j $(jobs) --path . --force

clean:
	cargo clean

version:
	verto
