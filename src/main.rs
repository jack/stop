use std::io::{self, Write};

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(setting = structopt::clap::AppSettings::ColoredHelp)]
#[structopt(rename_all = "kebab-case")]
/// Print text to the terminal until a specified stop word.
struct Cli {
    #[structopt()]
    /// Word to stop output on.
    word: String,
}

fn main() {
    let args = Cli::from_args();

    let word: Vec<char> = args.word.chars().collect();

    let mut current_char = 0;
    let max_char = word.len();

    loop {
        let mut input = String::new();

        let bytes = io::stdin()
            .read_line(&mut input)
            .expect("error reading stdin");

        // If we hit EOF without early-returning, we didn't find a match
        if bytes == 0 {
            eprintln!("word not found");

            return;
        }

        for char in input.chars() {
            // Create a byte array to hold the utf8 bytes
            let mut b = [0; 4];

            // Encode the character as utf8 bytes
            char.encode_utf8(&mut b);

            // Write the bytes to stdout
            io::stdout().write_all(&b).unwrap();

            // Check to see if this completed our searched word
            match char == word[current_char] {
                true => {
                    current_char += 1;
                    if current_char == max_char {
                        io::stdout()
                            .flush()
                            .expect("error flushing buffer to stdout");
                        eprintln!("\nreached searched word");
                        return;
                    }
                }
                false => current_char = 0,
            }
        }
    }
}
